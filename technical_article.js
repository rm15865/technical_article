
$(document).ready(function(){
  //initially check
  check();
  //check every time the window changes size
  $(window).resize(check);
});

function check(){
  //console.log("Col = "+$('#e1').css('background-color'));   //for testing purposes
  if ($('#e1').css('background-color') == "rgb(0, 255, 0)" ){
    updateDiv("Large");
  }else if ($('#e1').css('background-color') == "rgb(255, 0, 0)" ){
    updateDiv("Medium");
  }else if ($('#e1').css('background-color') == "rgb(255, 255, 0)" ){
    updateDiv("Small");
  }else if ($('#e1').css('background-color') == "transparent" ){
    updateDiv("Extra Small");
  }else{
    console.log("Problem.");
  }
}

function updateDiv(str){
  $('#f1').html("<p>f1 - Screen Size: "+str+".</p>");
}
